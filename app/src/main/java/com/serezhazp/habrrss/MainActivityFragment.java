package com.serezhazp.habrrss;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.cjj.MaterialRefreshLayout;
import com.cjj.MaterialRefreshListener;

import java.util.ArrayList;


public class MainActivityFragment extends Fragment implements AdapterView.OnItemClickListener {

    private enum RSSXMLTag {
        TITLE, DATE, LINK, IGNORETAG;
    }

    private final String HABR_RSS_URL = "https://habrahabr.ru/rss/hubs/";
    private final String GEEKTIMES_RSS_URL = "https://geektimes.ru/rss/hubs/";
    private final String MEGAMOZG_RSS_URL = "https://megamozg.ru/rss/hubs/";
    private final String HABR = "HABRAHABR";
    private final String GEEKTIMES = "GEEKTIMES";
    private final String MEGAMOZG = "MEGAMOZG";
    private final String LOG_TAG = "HABR_RSS";
    private PostItemAdapter mPostItemAdapter;
    private ListView mListView;
    private Intent mHabrapostIntent;
    private Intent mAboutIntent;
    private ConnectionDetector mConnectionDetector;
    private android.app.ActionBar mActionBar;
    private static MaterialRefreshLayout sRefreshLayout;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ArrayList<PostItem> itemList = new ArrayList<>();
        itemList.addAll(MainActivity.sDb.getAllPosts());

        View rootView = inflater.inflate(R.layout.refreshing_layout, container, false);

        mListView = (ListView) rootView.findViewById(R.id.listview_habr);
        mPostItemAdapter =
                new PostItemAdapter(getActivity(), R.layout.list_item, itemList);

        mListView.setAdapter(mPostItemAdapter);

        mListView.setOnItemClickListener(this);

        sRefreshLayout = (MaterialRefreshLayout) rootView.findViewById(R.id.refresh);
        sRefreshLayout.setWaveColor(0x528bc3d0);
        sRefreshLayout.setMaterialRefreshListener(new MaterialRefreshListener() {
            @Override
            public void onRefresh(MaterialRefreshLayout materialRefreshLayout) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        doNetworkJob();
                    }
                }).start();
            }
        });

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_refresh) {
            doNetworkJob();
            return true;
        } else if(id == R.id.clear_db) {
            MainActivity.sDb.clearDB();
            mPostItemAdapter.clear();
            return  true;
        } else if(id == R.id.about) {
            mAboutIntent = new Intent(getActivity(), AboutActivity.class);
            startActivity(mAboutIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_menu, menu);
    }

    public class HTTPAsyncTask extends AsyncTask<String, Integer, ArrayList<PostItem>> {
        private RSSXMLTag currentTag;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected ArrayList<PostItem> doInBackground(String... params) {
            ArrayList<PostItem> postItemList = new ArrayList<>();
            postItemList.clear();

            XMLParser xmlParser = new XMLParser();

            for(int i = 0; i < params.length; i++) {
                postItemList.addAll(xmlParser.getArticlesList(getActivity(), params[i], i));
            }

            return postItemList;
        }

        @Override
        protected void onPostExecute(ArrayList<PostItem> result) {
            sRefreshLayout.finishRefresh();
            if(result != null) {
                System.out.println(result.toString());
                ArrayList<PostItem> dbList = MainActivity.sDb.getAllPosts();
                if(dbList.size() == 0) {
                    MainActivity.sDb.addAllPosts(result);
                    mPostItemAdapter.addAll(MainActivity.sDb.getAllPosts());
                } else {
                    ArrayList<PostItem> newPostsList = getNewPosts(dbList, result);
                    if(newPostsList.size() > 0) {
                        MainActivity.sDb.addAllPosts(newPostsList);
                        mPostItemAdapter.clear();
                        mPostItemAdapter.addAll(MainActivity.sDb.getAllPosts());
                    }
                }
            }
            super.onPostExecute(result);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PostItem item = (PostItem) mListView.getItemAtPosition(position);
        String link = item.getPostLink();
        String title = item.getPostTitle();
        Bundle toWebView = new Bundle();
        toWebView.putString("link", link);
        toWebView.putString("title", title);
        mHabrapostIntent = new Intent(getActivity(), HabrapostViewActivity.class);
        mHabrapostIntent.putExtras(toWebView);
        mConnectionDetector = new ConnectionDetector(getActivity());
        if (mConnectionDetector.isConnectedToInternet()) {
            startActivity(mHabrapostIntent);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Внимание!")
                    .setMessage("Отсутствует подключение к интернету!")
                    .setCancelable(false)
                    .setNegativeButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
    }

    private ArrayList<PostItem> getNewPosts(ArrayList<PostItem> fromDB, ArrayList<PostItem> fromRSS) {
        ArrayList<PostItem> list;
        list = fromRSS;
        list.removeAll(fromDB);
        return list;
    }

    private void doNetworkJob() {
        mConnectionDetector = new ConnectionDetector(getActivity());
        if(mConnectionDetector.isConnectedToInternet()) {
            HTTPAsyncTask task = new HTTPAsyncTask();
            task.execute(HABR_RSS_URL, GEEKTIMES_RSS_URL, MEGAMOZG_RSS_URL);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Внимание!")
                    .setMessage("Отсутствует подключение к интернету!")
                    .setCancelable(false)
                    .setNegativeButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            sRefreshLayout.finishRefresh();
        }
    }


}
