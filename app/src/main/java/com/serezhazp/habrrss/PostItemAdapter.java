package com.serezhazp.habrrss;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class PostItemAdapter extends ArrayAdapter<PostItem> {
    private Activity mActivity;
    private ArrayList<PostItem> mItems;

    public PostItemAdapter(Context context, int textViewResourceId, ArrayList<PostItem> objects) {
        super(context, textViewResourceId, objects);

        mActivity = (Activity) context;
        mItems = objects;
    }

    static class ViewHolder {
        TextView postTitleView;
        TextView postDateView;
        TextView postTypeView;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            LayoutInflater inflater = mActivity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.postTitleView = (TextView) convertView.findViewById(R.id.articleTextView);
            viewHolder.postDateView = (TextView) convertView.findViewById(R.id.dateTextView);
            viewHolder.postTypeView = (TextView) convertView.findViewById(R.id.typeTextView);
            convertView.setTag(viewHolder);

            if(mItems.get(position).getPostType().equals("HABRAHABR"))
                convertView.setBackgroundResource(R.drawable.listview_selector);
            else if(mItems.get(position).getPostType().equals("GEEKTIMES"))
                convertView.setBackgroundResource(R.drawable.listview_selector2);
            else if(mItems.get(position).getPostType().equals("MEGAMOZG"))
                convertView.setBackgroundResource(R.drawable.listview_selector3);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            if(mItems.get(position).getPostType().equals("HABRAHABR"))
                convertView.setBackgroundResource(R.drawable.listview_selector);
            else if(mItems.get(position).getPostType().equals("GEEKTIMES"))
                convertView.setBackgroundResource(R.drawable.listview_selector2);
            else if(mItems.get(position).getPostType().equals("MEGAMOZG"))
                convertView.setBackgroundResource(R.drawable.listview_selector3);
        }

        viewHolder.postTitleView.setText(mItems.get(position).getPostTitle());
        viewHolder.postDateView.setText(dateLikeOnHabr(mItems.get(position).getPostDate()));
        viewHolder.postTypeView.setText(mItems.get(position).getPostType());

        return convertView;
    }

    private String dateLikeOnHabr(String date) {

        String result = "";

        DateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.US);
        Date today = new Date();
        Date parsed;
        parsed = convertStringToDate(date);

        Calendar calendarToday = Calendar.getInstance();
        calendarToday.setTime(today);
        Calendar calendarParsed = Calendar.getInstance();
        calendarParsed.setTime(parsed);

        if(calendarParsed.get(Calendar.YEAR) != calendarToday.get(Calendar.YEAR)) {
            result = dateGeneral(calendarParsed, dateFormat, parsed, true);
        } else {
            if(calendarParsed.get(Calendar.MONTH) == calendarToday.get(Calendar.MONTH)) {
                if(calendarParsed.get(Calendar.DAY_OF_MONTH) == calendarToday.get(Calendar.DAY_OF_MONTH))
                    result = "Сегодня в " + dateFormat.format(parsed);
                else if(calendarParsed.get(Calendar.DAY_OF_MONTH) == calendarToday.get(Calendar.DAY_OF_MONTH) - 1)
                    result = "Вчера в " + dateFormat.format(parsed);
                else
                    result = dateGeneral(calendarParsed, dateFormat, parsed, false);
            } else
                result = dateGeneral(calendarParsed, dateFormat, parsed, false);
        }

        return result;
    }

    private String dateGeneral(Calendar calendar, DateFormat dateFormat, Date date, boolean withYear) {
        String result = "";
        if(!withYear) {
            result = calendar.get(Calendar.DAY_OF_MONTH)
                    + " "
                    + rusMonth(calendar.get(Calendar.MONTH))
                    + " в "
                    + dateFormat.format(date);
        } else {
            result = calendar.get(Calendar.DAY_OF_MONTH)
                    + " "
                    + rusMonth(calendar.get(Calendar.MONTH))
                    + " "
                    + calendar.get(Calendar.YEAR)
                    + " в "
                    + dateFormat.format(date);
        }
        return result;
    }

    private String rusMonth(int month) {
        String result = "";
        switch (month + 1){
            case 1: result = "января";
                break;
            case 2: result = "февраля";
                break;
            case 3: result = "марта";
                break;
            case 4: result = "апреля";
                break;
            case 5: result = "мая";
                break;
            case 6: result = "июня";
                break;
            case 7: result = "июля";
                break;
            case 8: result = "августа";
                break;
            case 9: result = "сентября";
                break;
            case 10: result = "октября";
                break;
            case 11: result = "ноября";
                break;
            case 12: result = "декабря";
                break;
            default:
                break;
        }
        return result;
    }

    public static Date convertStringToDate(String input) {
        Date date = null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        try {
            date = dateFormat.parse(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }
}
