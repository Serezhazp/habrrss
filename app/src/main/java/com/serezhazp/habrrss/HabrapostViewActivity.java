package com.serezhazp.habrrss;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


public class HabrapostViewActivity extends Activity {

    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.habrapost_view);

        createWebView(getIntent().getExtras());
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    private void createWebView(Bundle bundle) {
        String prefix = "m.";
        String link = bundle.getString("link");
        try {
            link = link.substring(0, 7) + prefix + link.substring(8, link.length());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        final String title = bundle.getString("title");
        Toast.makeText(this, "LINK: " + link, Toast.LENGTH_SHORT).show();
        mWebView = (WebView) this.findViewById(R.id.webview);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        final Activity activity = this;
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView webView, int progress) {
                activity.setTitle(title);
                activity.setProgress(progress * 100);

                if (progress == 100)
                    activity.setTitle(title);
            }
        });
        mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getApplicationContext(),
                        "Error: " + description + " " + failingUrl,
                        Toast.LENGTH_LONG)
                        .show();
            }
        });

        mWebView.loadUrl(link);
    }
}
