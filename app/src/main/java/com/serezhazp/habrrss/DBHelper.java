package com.serezhazp.habrrss;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "HabrRssDB";
    private static final String TABLE_HABRRSS = "habrrss";
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_LINK = "link";
    private static final String KEY_DATE = "date";
    private static final String KEY_TYPE = "type";

    private static final String[] COLUMNS = {
            KEY_ID,
            KEY_TITLE,
            KEY_LINK,
            KEY_DATE,
            KEY_TYPE,
    };

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_HABRRSS + " ( " +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                KEY_TITLE + " TEXT, " +
                KEY_LINK + " TEXT, " +
                KEY_DATE + " TEXT, " +
                KEY_TYPE + " TEXT )";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABLE_HABRRSS);
        this.onCreate(db);
    }

    public void addPost(PostItem postItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, postItem.getPostTitle());
        values.put(KEY_LINK, postItem.getPostLink());
        values.put(KEY_DATE, postItem.getPostDate());
        values.put(KEY_TYPE, postItem.getPostType());

        db.insert(TABLE_HABRRSS, null, values);

        db.close();
    }

    public void addAllPosts(ArrayList<PostItem> list) {
        for(PostItem item : list) {
            addPost(item);
        }
    }

    public PostItem getPost(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor =
                db.query(TABLE_HABRRSS,
                        COLUMNS,
                        " " + KEY_ID + " = ?",
                        new String[]{String.valueOf(id)},
                        null,
                        null,
                        null,
                        null);

        PostItem item = new PostItem();
        if(cursor.moveToLast()) {
            item.setPostTitle(cursor.getString(1));
            item.setPostLink(cursor.getString(2));
            item.setPostDate(cursor.getString(3));
            item.setPostType(cursor.getString(4));
        }
        db.close();
        cursor.close();

        return item;
    }

    public ArrayList<PostItem> getAllPosts() {
        ArrayList<PostItem> postsList = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_HABRRSS +
                " ORDER BY datetime(" + KEY_DATE + ") DESC";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        PostItem item;
        if (cursor.moveToFirst()) {
            do {
                item = new PostItem();
                item.setPostTitle(cursor.getString(1));
                item.setPostLink(cursor.getString(2));
                item.setPostDate(cursor.getString(3));
                item.setPostType(cursor.getString(4));

                postsList.add(item);
            } while (cursor.moveToNext());
        }
        db.close();
        cursor.close();
        return postsList;
    }

    public void clearDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_HABRRSS);
        db.close();
    }
}
