package com.serezhazp.habrrss;


public class PostItem {
    private long mId;
    private String mPostTitle;
    private String mPostDate;
    private String mPostLink;
    private String mPostType;

    public PostItem() {}

    public PostItem(String postTitle, String postDate, String postLink) {
        this.mPostTitle = postTitle;
        this.mPostDate = postDate;
        this.mPostLink = postLink;
    }

    public PostItem(long id, String postTitle, String postDate, String postLink) {
        this.mId = id;
        this.mPostTitle = postTitle;
        this.mPostDate = postDate;
        this.mPostLink = postLink;
    }

    //TODO: Make the constructor with mPostType

    public void setId(long id) {
        this.mId = id;
    }
    public long getId() {
        return mId;
    }
    public void setPostTitle(String postTitle) {
        this.mPostTitle = postTitle;
    }
    public String getPostTitle() {
        return mPostTitle;
    }
    public void setPostLink(String postLink) {
        this.mPostLink = postLink;
    }
    public String getPostLink() {
        return mPostLink;
    }
    public void setPostDate(String postDate) {
        this.mPostDate = postDate;
    }
    public String getPostDate() {
        return mPostDate;
    }
    public void setPostType(String postType) {
        this.mPostType = postType;
    }
    public String getPostType() {
        return mPostType;
    }


    @Override
    public int hashCode() {
        int hash = 42;
        hash = 7 * hash * this.mPostLink.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        boolean result = false;
        if(object == null || object.getClass() != getClass()) {
            result = false;
        } else {
            PostItem postItem = (PostItem) object;
            if (this.mPostLink.equals(postItem.getPostLink())
                    && this.mPostDate.equals(postItem.getPostDate())) {
                result = true;
            }
        }
        return result;
    }


    @Override
    public String toString() {
        return mPostDate + "\n" + mPostTitle;
    }
}
