package com.serezhazp.habrrss;

import android.content.Context;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class XMLParser {
    private enum RSSXMLTag {
        TITLE, DATE, LINK, IGNORETAG, INIT;
    }

    public XMLParser() {
    }

    public ArrayList<PostItem> getArticlesList(Context context,
                                               String siteUrl,
                                               int intTag){
        InputStream inputStream;
        RSSXMLTag currentTag = RSSXMLTag.INIT;
        ArrayList<PostItem> postItemList = new ArrayList<>();

        try {
            URL url = new URL(siteUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10 * 900);
            connection.setConnectTimeout(10 * 900);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            int response = connection.getResponseCode();
            if(response != 200) {
                Log.d("Response code != 200: ", String.valueOf(response));
            } else {
                inputStream = connection.getInputStream();

                //parse XML
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser parser = factory.newPullParser();
                parser.setInput(inputStream, null);

                int eventType = parser.getEventType();
                PostItem postItemData = null;
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_DOCUMENT) {

                    } else if (eventType == XmlPullParser.START_TAG) {
                        if (parser.getName().equals("item")) {
                            postItemData = new PostItem();
                            postItemData.setPostType(getTag(intTag));
                            currentTag = RSSXMLTag.IGNORETAG;
                        } else if (parser.getName().equals("title")) {
                            currentTag = RSSXMLTag.TITLE;
                        } else if (parser.getName().equals("link")) {
                            currentTag = RSSXMLTag.LINK;
                        } else if (parser.getName().equals("pubDate")) {
                            currentTag = RSSXMLTag.DATE;
                        }
                    } else if (eventType == XmlPullParser.END_TAG) {
                        if (parser.getName().equals("item")) {
                            postItemData.setPostDate(changeDateFormat(postItemData.getPostDate()));
                            postItemList.add(postItemData);
                        } else {
                            currentTag = RSSXMLTag.IGNORETAG;
                        }
                    } else if (eventType == XmlPullParser.TEXT) {
                        String content = parser.getText();
                        content = content.trim();
                        if (postItemData != null) {
                            switch (currentTag) {
                                case TITLE:
                                    if (content.length() != 0) {
                                        if (postItemData.getPostTitle() != null) {
                                            postItemData.setPostTitle(postItemData.getPostTitle() + content);
                                        } else {
                                            postItemData.setPostTitle(content);
                                        }
                                    }
                                    break;
                                case LINK:
                                    if (content.length() != 0) {
                                        if (postItemData.getPostLink() != null) {
                                            postItemData.setPostLink(postItemData.getPostLink() + content);
                                        } else {
                                            postItemData.setPostLink(content);
                                        }
                                    }
                                    break;
                                case DATE:
                                    if (content.length() != 0) {
                                        if (postItemData.getPostDate() != null) {
                                            postItemData.setPostDate(postItemData.getPostDate() + content);
                                        } else {
                                            postItemData.setPostDate(content);
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    eventType = parser.next();
                }
            }
        } catch (MalformedURLException e) {
            Log.e("Error", "MalformedURLException");
        } catch (ProtocolException e) {
            Log.e("Error", "ProtocolException");
        } catch (XmlPullParserException e) {
            Log.e("Error", "XmlPullParserException");
        }  catch (IOException e) {
            Log.e("Error", "IOException");
            e.printStackTrace();
        }

        return postItemList;
    }

    private String getTag(int intTag) {
        String[] tag = {
                "HABRAHABR",
                "GEEKTIMES",
                "MEGAMOZG"
        };
        return tag[intTag];
    }

    private String changeDateFormat(String date) {
        String result;
        Date d = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.US);
        try {
            d = dateFormat.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        dateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        result = dateFormat.format(d);

        return result;
    }
}
